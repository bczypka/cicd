# CI/CD With ArgoCD and GitOps

## Gameplan

1. Create an Application that should be run in ArgoCD. In this case it will be a super simple API (see `src/`)

2. Create a Gitlab Repo for the application

3. Create `Dockerfile`
4. Set up `.gitlab-ci.yml`
5. Create `deployment/` directory with Kubernetes Manifests
6. Setup Kubernetes Cluster (e.g. minikube and kubectl)
   - `minikube start` (start K8s cluster)
   - `kubectl get nodes` (check runnning nodes)
   - `minikube addons enable ingress` (add ingress addon)
   - `kubectl create namespace argocd` (create ArgoCD namespace)
