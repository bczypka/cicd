FROM python:3.9

WORKDIR /app

# Install poetry
RUN pip install poetry

COPY pyproject.toml poetry.lock ./

# Install only dependencies:
RUN poetry config virtualenvs.create false \
  && poetry install --no-interaction --no-ansi --no-root --no-dev

# Copy in everything else
COPY . .

RUN poetry install --no-dev

# Run server
CMD [ "poetry", "run", "uvicorn", "--host", "0.0.0.0", "src.api:app" , "--reload" ]