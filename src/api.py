from fastapi import FastAPI

version = "v4"

app = FastAPI()

@app.get(f"/")
async def root():
	return {"message": f"Hello from {version}"}
